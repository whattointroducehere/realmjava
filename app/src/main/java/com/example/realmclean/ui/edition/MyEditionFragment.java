package com.example.realmclean.ui.edition;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.realmclean.R;
import com.example.realmclean.model.MyBook;


import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;


    public  class MyEditionFragment extends Fragment implements MyEditionFragmentContract.View {

        private MyEditionFragmentContract.Presenter presenter;

        EditText mEditTitle;

        private Realm mRealm;

        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            RealmConfiguration config = new RealmConfiguration.Builder()
                    .build();
            mRealm = Realm.getInstance(config);
        }

        @Nullable
        @Override
        public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_edition, container, false);

            return view;
        }


        @Override
        public void onDestroy() {
            super.onDestroy();
            mRealm.close();
        }
          @Override
        public void onAddCLick() {
            mRealm.beginTransaction();
            MyBook book = mRealm.createObject(MyBook.class);
            book.setTitle(getTrimmedTitle());
            mRealm.commitTransaction();
        }

        @Override
        public void onRemoveClick() {
            mRealm.beginTransaction();
            RealmResults<MyBook> books = mRealm.where(MyBook.class).equalTo("title", getTrimmedTitle()).findAll();
            if(!books.isEmpty()) {
                for(int i = books.size() - 1; i >= 0; i--) {
                    books.get(i).deleteFromRealm();

                }
            }
            mRealm.commitTransaction();
        }

        private String getTrimmedTitle() {
            return mEditTitle.getText().toString().trim();
        }
}





