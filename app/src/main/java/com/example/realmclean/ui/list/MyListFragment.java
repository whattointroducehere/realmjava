package com.example.realmclean.ui.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.realmclean.R;
import com.example.realmclean.model.MyBook;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;



public class MyListFragment extends Fragment {
    RecyclerView mRecyclerView;
    private Realm mRealm;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .build();
        mRealm = Realm.getInstance(config);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        ArrayList<MyBook> list = new ArrayList<>();
        mRealm.executeTransaction(realm -> {
            list.addAll(realm.copyFromRealm(mRealm.where(MyBook.class).findAll()));
        });
        mRecyclerView.setAdapter(new MyListAdapter(list));

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }
}


