package com.example.realmclean.ui.edition;

import com.example.realmclean.presentation.base.BasePresenter;

public interface MyEditionFragmentContract {

    interface View{

        void onAddCLick();

        void onRemoveClick();

    }

    interface Presenter extends BasePresenter {

        void eventClickAdd();

        void eventClickRemove();
    }
}


