package com.example.realmclean.ui.edition;

public class MyEditionFragmentPresenter implements MyEditionFragmentContract.Presenter {

    private MyEditionFragmentContract.View view;


    @Override
    public void OnStartView(Object view) {


    }

    @Override
    public void OnStopView() {
        view = null;
    }

    @Override
    public void eventClickAdd() {
        view.onAddCLick();
    }

    @Override
    public void eventClickRemove() {
        view.onRemoveClick();

    }
}



