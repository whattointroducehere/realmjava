package com.example.realmclean.model;

import io.realm.RealmObject;
import io.realm.annotations.Required;

public class MyBook extends RealmObject {

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Required

    private String title;
}



