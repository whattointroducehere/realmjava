package com.example.realmclean.presentation;

import com.example.realmclean.presentation.base.BasePresenter;

public interface IPresenter {

    interface View{

    }

    interface EventListener extends BasePresenter<View> {

        void init();

    }
}



