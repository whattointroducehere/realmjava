package com.example.realmclean.presentation;


public class Presenter implements IPresenter.EventListener {
    private IPresenter.View view;


    @Override
    public void OnStartView(IPresenter.View view) {
        this.view=view;

    }

    @Override
    public void OnStopView() {
        if (view != null) view = null;


    }

    @Override
    public void init() {

    }


}


