package com.example.realmclean.presentation;

import android.os.Bundle;

import com.example.realmclean.R;
import com.example.realmclean.databinding.ActivityMainBinding;
import com.example.realmclean.presentation.base.BaseActivity;
import com.example.realmclean.presentation.base.BasePresenter;
import com.example.realmclean.ui.edition.MyEditionFragment;
import com.example.realmclean.ui.list.MyListFragment;

import io.realm.Realm;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements IPresenter.View {

    private IPresenter.EventListener presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Realm.init(this);

        if(savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.edition_container, new MyEditionFragment())
                    .add(R.id.list_container, new MyListFragment())
                    .commit();
        }
    }



    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }
    
    @Override
    protected void initView() {
        presenter = new Presenter();
        getBinding().setEvent(presenter);
        presenter.init();
    }

    @Override
    protected void onDestroyView() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }
}
