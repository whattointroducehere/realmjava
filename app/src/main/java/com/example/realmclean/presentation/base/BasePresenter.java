package com.example.realmclean.presentation.base;




public interface BasePresenter<V> {
    void OnStartView(V view);

    void OnStopView();

}


